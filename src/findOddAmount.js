function findOddAmount(numbers) {

    var count;

    for(var i = 0; i < numbers.length; i++){

        count = 0;

        //Counting the frequency of the value
        numbers.forEach(function (value) {
           if(value === numbers[i]){
               count++;
           }
        });

        //Check if the count is odd if yes then return the value.
        if(count%2 !== 0){
            return numbers[i];
        }
    }

}

//Exporting the module.
module.exports = findOddAmount;
