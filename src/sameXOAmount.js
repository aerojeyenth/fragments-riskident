function sameXOAmount(text) {
    
    //Finding the no of x's and o's by using javascript match with regex.
    var xs = text.match(/x/gi) || [];
    var os = text.match(/o/gi) || [];

    return xs.length == os.length;
}

//Exporting the module.
module.exports = sameXOAmount;
