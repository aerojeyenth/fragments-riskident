function sumSmallestNumbers(numbers) {

    //Sort the array.
    var arraySort = numbers.sort(function(a, b){return a-b});

    //Return the sum of the 1st and 2nd value of the sorted array.
    return arraySort[0] + arraySort[1];
}

function sumSmallestNumbersAlternate(numbers) {

    var first = numbers[0];
    var second = numbers[1];

    if(first < second){
        first = numbers[1];
        second = numbers[0];
    }

    numbers.forEach(function (value) {
        if(value < first){
            second = first;
            first = value;
        }else if(value < second){
            second = value;
        }
    });

    return first + second;
}

//Exporting the module.
module.exports = {
    main: sumSmallestNumbers,
    alternate: sumSmallestNumbersAlternate
};
